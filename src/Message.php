<?php
namespace Newdir\EximServer;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use \Newdir\EximServer\Log;
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 24.08.2018
 * Time: 14:48
 */

class Message
{
    private $db = null;
    private $type = 'message';

    //Поля письма согласно RFC-822
    private $message_id;        //Уникальный идентификатор письма (при отправке с exim-сервера к нему будет добавлен id получателя)
    private $to = array();      //The array of 'to' names and addresses.

    //Дополнительные атрибуты
    private $enableTestMode;        //Активирует режим тестирования (Определим его попозже)
    private $mail = null;
    private $list_name = null;

    private $members = [];
    private $html = null;


    public function __construct($params)
    {
        $this->message_id = $params['message_id'];
        $this->type = $params['type'];
        $this->to = $params['to'];
        $this->enableTestMode = (isset($params['enableTestMode'])) ? $params['enableTestMode'] : false;
        $this->members = (isset($params['member'])) ? [$params['member']] : null;

        if (isset($params['list'])) {
            $this->members = $params['list']['members'];
            $this->list_name = $params['list']['name'];
        }

        $this->db = $params['db'];

        $mail = new PHPMailer(true);
        $mail->addCustomHeader('Precedence', 'bulk');
        $mail->addCustomHeader('List-Unsubscribe', '<mailto:info@newdirections.ru?body=unsubscribe>');
        $mail->addCustomHeader('List-id', time());
        $mail->Sender = 'info@newdirections.ru';
        $mail->CharSet = 'UTF-8';
        $mail->isSendmail();
        list($address, $name) = $params['from'];
        $mail->setFrom($address, $name);

        if (isset($params['replyTo']) && $params['replyTo']) {
            list($address, $name) = $params['replyTo'];
            $mail->addReplyTo($address, $name);
        } else {
            $mail->addReplyTo($address, $name);
        }

        if (isset($params['bcc']) && $params['bcc']) {
            list($address, $name) = $params['bcc'];
            $mail->addBCC($address, $name);
        }

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $params['subject'];
        //$mail->Body    = $params['html'];
        $this->html = $params['html'];
        $mail->AltBody = strip_tags($params['html']);

        $mail->MessageID = $this->message_id;

        if (isset($params['php_mailer_params']) &&  $params['php_mailer_params']) {

            foreach ($params['php_mailer_params'] as $key => $param) {
                $mail->$key = $param;
            }
        }
        $this->mail = $mail;
    }


    public function send()
    {
        switch ($this->type) {

            case 'message' :
                return $this->sendMessage();

            case 'member_message' :
                $member = array_pop($this->members);
                return $this->sendMemberMessage($member);

            case 'list_message' :
                return $this->sendListMessage();
        }
    }


    private function sendMessage()
    {
        $mail = $this->mail;
        try {
            list($address, $name) = $this->to;
            $mail->addAddress($address, $name);
            return $this->sendMail($mail);

        } catch (Exception $e) {
            $this->logError($mail, $e);
            return false;
        }
    }


    private function sendMemberMessage($member)
    {
        $mail = $this->mail;
        try {
            $mail->clearAddresses();
            $mail->addAddress($member['address'], isset($member['name']) ? $member['name'] : '');
            $mail->MessageID = '<' . $member['id'] . '_' . $this->message_id  . '@mail1.newdirections.ru' . '>';
            return $this->sendMail($mail);

        } catch (Exception $e) {

            $this->logError($mail, $e);
            return false;
        }
    }


    private function sendListMessage()
    {
        foreach ($this->members as $member) {
            $this->sendMemberMessage($member);
        }

        return ['success' => true];
    }


    private function sendMail($mail)
    {
        $to = $mail->getToAddresses();
        $recipient_email = $to[0][0];

        //*** twig templating
        $loader = new \Twig\Loader\ArrayLoader(array(
            'index' => $this->html,
        ));
        $twig = new \Twig\Environment($loader);
        $mail->Body = $twig->render('index', array('recipient_email' => $recipient_email));
        //***

        $result = $mail->send();
        $db = $this->db;

        $message_id = substr($mail->MessageID, 1, strpos($mail->MessageID, '@') - 1);

        $db->saveMessageId($message_id, $this->list_name);

        return ($result) ? ['success' => true] : ['success' => false, 'message' => $mail->ErrorInfo];
    }


    private function log($type, $text)
    {
        $logger = new Log($type);
        $logger->log($text);
    }


    private function logError($mail, Exception $e)
    {
        $this->log(Log::ERROR, $mail->MessageID . ' ' . $e->getTraceAsString());
    }

}