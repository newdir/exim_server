<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 14.09.2018
 * Time: 14:27
 */

namespace Newdir\EximServer;


class MessageState
{

    const MESSAGE_STATE_NEW = 0;

    const MESSAGE_STATE_SUCCESS = 1;

    const MESSAGE_STATE_PENDING = 2;

    const MESSAGE_STATE_ERROR = 3;


    public $final_states = array(self::MESSAGE_STATE_SUCCESS);


    public function get($data)
    {
        $state_id = null;

        if (preg_match('/\*\*/', $data)) {
            $state_id = self::MESSAGE_STATE_ERROR;
        } elseif (preg_match('/Completed/', $data)) {
            $state_id = self::MESSAGE_STATE_SUCCESS;
        }

        return $state_id;
    }
}