<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 04.09.2018
 * Time: 10:00
 */
namespace Newdir\EximServer;

use SQLite3;

class Db
{

    private $db_name;


    public function __construct($db_name)
    {
        $this->db_name = $db_name;
    }

    public function getResource()
    {
        return new SQLite3($this->db_name);
    }


    public function getMessageIdList()
    {
        $resource = $this->getResource();
        $query = $resource->query("select message_id, list_name, attempt from message where state_id=0");
        $result = [];

        while($res = $query->fetchArray(SQLITE3_ASSOC)){
            $result[] = $res;
        }

        $resource->close();
        return $result;
    }


    public function getMessageLogList($params)
    {
        $resource = $this->getResource();

        $where = [];

        $where[] = isset($params['date']) ? "date>=" . $params['date'] : null;
        $where[] = isset($params['message_id']) ? "message_id='" . $params['message_id'] . "'" : null;
        $where[] = isset($params['list_name']) ? "list_name='" . $params['list_name'] . "'" : null;

        //$where[] = 'state_id not in (' . implode(',', MessageState::final_states) . ')';

        $where = array_filter($where);

        if (! count($where)) {
            return [];
        }

        $query = $resource->query('select message_id, state_id, list_name from message where ' . implode(' and ', $where));
        $result = [];

        while($res = $query->fetchArray(SQLITE3_ASSOC)){
            $result[] = $res;
        }
        $resource->close();

        return $result;
    }


    public function saveMessageIdLog($message, $data, $state_id = null)
    {
        $resource = $this->getResource();
        $message_id = strtr($message['message_id'], ['<' => '', '>' => '']);
        $list_name = $message['list_name'];

        $resource->query("insert into log(message_id, list_name, data, state_id) values('$message_id', '$list_name' ,'$data', '$state_id')");

        $message['attempt'] += $message['attempt'];
        $update_columns = [];
        $update_columns[] = "attempt=" . $message['attempt'];

        if ($state_id) {
            $update_columns[] = 'state_id=' . $state_id;
        }

        $query = "update message set " . implode(',', $update_columns) . " where message_id='" . $message['message_id'] . "'";
        $resource->query($query);

        $resource->close();
    }


    public function saveMessageId($message_id, $list_name)
    {
        $resource = $this->getResource();
        $resource->query("insert into message(message_id, list_name, date, attempt, state_id) values('$message_id', '$list_name', ". time() . ",0, 0)");
        $resource->close();
    }


    public function initDb()
    {
        $resource = $this->getResource();
        $resource->query("create table log(message_id TEXT, list_name TEXT, data TEXT, date INTEGER, state_id INTEGER)");
        $resource->query("create table message(message_id TEXT, list_name TEXT, date INTEGER, attempt INTEGER, state_id INTEGER)");
        $resource->close();
    }


    public function clearDb()
    {
        $resource = $this->getResource();
        $resource->query("drop table log");
        $resource->query("drop table message");
        $resource->close();
    }
}