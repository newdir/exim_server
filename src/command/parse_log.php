<?php
// TODO максимально нормализовать данные по логам, чтобы снизить требования к объему хранилища
// TODO Проработать все возможные статусы отправки письма, возвращаемые от почтовика
// TODO Подумать об ускорении и параллельном выполнении

defined('ROOT_PATH') 		|| define('ROOT_PATH', dirname(__FILE__) . '/../../../../../');

function getDataByMessageId($message)
{
    $path_to_log = ROOT_PATH . 'protected/data/exim_logs/';
    $list_files = scandir($path_to_log);

    $message_id =  strtr($message['message_id'],['<' => '', '>' => '', ' '=>'']);
    $message_id = preg_replace('/\s/', '', $message_id);

    echo $message_id . PHP_EOL;

    $res = '';

    foreach ($list_files as $filename)
    {
        if (in_array($filename,['.', '..'])) {
            continue;
        }

        $file_path = $path_to_log . $filename;

        if ('gz' == pathinfo($filename, PATHINFO_EXTENSION)) {

            $path_to_tmp = ROOT_PATH . '/protected/temp/';
            $file_path_tmp = $path_to_tmp . $filename . '.txt';

            if (! file_exists($file_path_tmp)) {
                $zd = gzopen($file_path, "r");
                $data = gzread($zd, 100000);
                file_put_contents($file_path_tmp, $data);
            }
            $file_path = $file_path_tmp;
        }

        exec( "/usr/sbin/exigrep $message_id $file_path", $output);

        if ($output) {

            $res .= implode('|', $output);

        }

        if (preg_match('/Completed/', $res)) {
            break;
        }

        $output = '';
    }

    return $res;
}

date_default_timezone_set('Europe/Moscow');
require_once ROOT_PATH . 'vendor/autoload.php';

$db = new \Newdir\EximServer\Db(ROOT_PATH . '/protected/data/exim_db');
$list = $db->getMessageIdList();

foreach ($list as $item)
{
    $data = getDataByMessageId($item);
    $ms = new \Newdir\EximServer\MessageState();
    $db->saveMessageIdLog($item, $data, $ms->get($data));
}
