#!/bin/bash

exim_log_path="/var/log/exim4/*"
#exim_log_path=`dirname "$0"`"/../../../../../protected/dummy/"
tmp_dir_name="exim_"`date +"%y%m%d%H%M"`"/"
path_to_tmp_dir=`dirname "$0"`"/../../../../../protected/data/exim_logs/"
#TODO сделать копирование только сегодняшних файлов
find $exim_log_path -mtime -1 -type f -exec cp {} $path_to_tmp_dir \;
#cp -rf $exim_log_path $path_to_tmp_dir
php `dirname "$0"`"/parse_log.php"
mkdir $path_to_tmp_dir"../recycle/"$tmp_dir_name
recycled=$path_to_tmp_dir"../recycle/"$tmp_dir_name
path_to_tmp_dir=$path_to_tmp_dir"*"
mv $path_to_tmp_dir $recycled