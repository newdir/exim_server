<?php
/**
 * Class Log
 * A really simple logging class that writes flat data to a file.
 * @author Dennis Thompson
 * @license MIT
 * @version 1.0
 * @copyright AtomicPages LLC 2014
 */
namespace Newdir\EximServer;


class Log {

    const ERROR = 'error';

    const MESSAGE_ID = 'message_id';

    private $handle, $dateFormat, $type;

    public function __construct($type)
    {
        $this->dateFormat = "d/M/Y H:i:s";
        $this->type = $type;
    }

    public function dateFormat($format)
    {
        $this->dateFormat = $format;
    }

    public function getDateFormat()
    {
        return $this->dateFormat;
    }
    /**
     * Writes info to the log
     * @param mixed, string or an array to write to log
     * @access public
     */
    public function log($entries)
    {
        $filename = ROOT_PATH . '/protected/log/' . $this->type . '.txt';

        if(is_string($entries)) {
            $this->writeToFile($filename, $entries);
        } else {
            foreach($entries as $value) {
                $this->writeToFile($filename, $value);
            }
        }
    }


    private function writeToFile($filename, $string)
    {
        if (! file_exists($filename)) {
            touch($filename);
        }
        file_put_contents($filename, "[" . date($this->dateFormat) . "] " . $string, FILE_APPEND);
    }
}