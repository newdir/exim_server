<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 24.08.2018
 * Time: 14:49
 */
namespace Newdir\EximServer;


use Newdir\EximServer\Message;


class Queue
{

    const BLOCK_COUNT = 100;


    public function getList()
    {
        return [];
    }


    public function append($message)
    {
        $this->saveMessage($message);
        if ($message['type'] == 'message_list' && count($message['members']))
        {
            $this->saveMessageList($message['id'], $message['members']);
        }

        return $this;
    }


    public function run()
    {
        $block = $this->getBlock();
        if (! count($block)) {
            return;
        }

        foreach ($block as $message) {

            $m = new Message($message);
            $m->send();
            //Отметить как обработанное
        }
    }


    /**
     * @return $this
     * необходимо объединить сущности message и member_list, отсортировать согласно приоритету письма (убывание), времени добавления (возрастание)
     */
    private function getBlock()
    {
        return [];
    }


    private function saveMessage($message)
    {
        return $this;
    }


    private function saveMessageList($message_id, $members)
    {

    }
}